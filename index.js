const brain = require('brain.js');
const fs = require('fs');
const jimp = require('jimp');

function getPixelFromImagePath(imagePath) {
    return new Promise((resolve, _) => {
        jimp.read(imagePath, function (err, image) {
            if (err) _(err);
            let result = [];

            for (let i = 0; i < 64; i++) {
                for (let j = 0; j < 64; j++) {
                    result.push(image.getPixelColor(i, j) == 4294967295 ? 0 : 1);
                }
            }

            resolve(result);
        });
    })
}

(async function () {
    let trainData = [
        {
            input: await getPixelFromImagePath('./images/number-one.png'),
            output: {
                number_1: 1
            }
        },
        {
            input: await getPixelFromImagePath('./images/number-one-2.png'),
            output: {
                number_1: 1
            }
        },
        {
            input: await getPixelFromImagePath('./images/number-one-test.png'),
            output: {
                number_1: 1
            }
        },
        {
            input: await getPixelFromImagePath('./images/number-two.png'),
            output: {
                number_2: 1
            }
        },
        {
            input: await getPixelFromImagePath('./images/number-three.png'),
            output: {
                number_3: 1
            }
        },
        {
            input: await getPixelFromImagePath('./images/number-four.png'),
            output: {
                number_4: 1
            }
        }
    ];

    const net = new brain.NeuralNetworkGPU();

    net.train(trainData, {
        activation: 'leaky-relu',
        log: true
    });

    const test_pixels = await getPixelFromImagePath('./images/number-four-2.png');

    const result = brain.likely(test_pixels, net);

    console.log(result);
})();
